﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SocialPlatforms.Impl;
using UnityEngine.UI;

public class Main : MonoBehaviour
{
    [SerializeField] private GameObject StartBTN;
    [SerializeField] private Text ScoreText;
    public Cube Cube;

    // Start is called before the first frame update
    void Start()
    {
        Time.timeScale = 0f;
    }

    // Update is called once per frame
    void Update()
    {
        ScoreText.text = Cube.GetScore().ToString();
    }

    public void StartGame()
    {
        StartBTN.SetActive(false);
        Time.timeScale = 1f;
    }
}
