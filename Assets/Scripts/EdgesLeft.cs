﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EdgesLeft : MonoBehaviour
{
    [SerializeField] private float Speed;
    [SerializeField] public bool hitRight = false;
    [SerializeField] public bool hitLeft = false;
    [SerializeField] private Transform pointLeft;



    // Start is called before the first frame update
    void Start()
    {
        
            pointLeft.position = new Vector3(transform.position.x + 0.38f, transform.position.y, transform.position.z);

    }


    // Update is called once per frame
    void Update()
    {
        transform.position = Vector3.MoveTowards(transform.position, pointLeft.position, Speed * Time.deltaTime);

        if (hitRight && !hitLeft)
        {
            pointLeft.transform.position = new Vector3(transform.position.x + 0.38f, transform.position.y, transform.position.z);
                hitRight = false;
        }

        else if (hitLeft && !hitRight)
        {
            pointLeft.transform.position = new Vector3(transform.position.x - 0.38f, transform.position.y, transform.position.z);
            hitLeft = false;
        }
        
    }
    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.tag == "Player")
        {
            Invoke("Lose", 1f);
            Destroy(collision.gameObject);
            SceneManager.LoadScene(0);

        }
    }

}
