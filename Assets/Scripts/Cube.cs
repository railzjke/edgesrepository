﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.SocialPlatforms.Impl;
using UnityEngine.UI;

public class Cube : MonoBehaviour
{
    private Rigidbody rb;
    [SerializeField] private int Hight = 2;
    [SerializeField] private float Speed = 2;
    [SerializeField] public int Score;
    [SerializeField] private bool _left = true;
    public Transform posLeft;
    public Transform posRight;
    public EdgesLeft[] edLeft;
    public EdgesRight[] edRight;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
       // rb.velocity = Vector3.left * Speed;

       
    }

    // Update is called once per frame
    void Update()
    {
        if (_left)
            transform.parent.Translate(Vector3.left * Speed * Time.deltaTime);
        else
            transform.parent.Translate(Vector3.right * Speed * Time.deltaTime);

    }
    public void PushUp()
    {
 
        rb.velocity= Vector3.up * Hight;
       // rb.AddForce(Vector3.up * Hight, ForceMode.VelocityChange);
       
    }
    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "LeftWall")
        {
            for (int i = 0; i < edLeft.Length; i++)
            {
                edLeft[i].hitRight = false;
                edLeft[i].hitLeft = true;
                edRight[i].hitRight = false;
                edRight[i].hitLeft = true;
            }
            _left = false;
            Score++;
        }
        else if (collision.gameObject.tag == "RightWall")
        {
            for ( int i = 0; i < edLeft.Length; i++)
            {
                edLeft[i].hitRight = true;
                edLeft[i].hitLeft = false;
                edRight[i].hitRight = true;
                edRight[i].hitLeft = false;
            }

            _left = true;
            Score++;
           

        }


    }
    public int GetScore()
    {
        return Score;
    }

}
