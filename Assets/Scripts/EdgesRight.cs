﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EdgesRight : MonoBehaviour
{
    [SerializeField] private float Speed;
    [SerializeField] public bool hitRight = false;
    [SerializeField] public bool hitLeft = false;
    [SerializeField] private Transform pointRight;



    // Start is called before the first frame update
    void Start()
    {
        
            pointRight.position = new Vector3(transform.position.x, transform.position.y, transform.position.z);

    }


    // Update is called once per frame
    void Update()
    {
        transform.position = Vector3.MoveTowards(transform.position, pointRight.position, Speed * Time.deltaTime);

        if (hitLeft && !hitRight)
        {
            pointRight.transform.position = new Vector3(transform.position.x - 0.38f, transform.position.y, transform.position.z);
            hitLeft = false;
        }
        else if (hitRight && !hitLeft)
        {
            pointRight.transform.position = new Vector3(transform.position.x + 0.38f, transform.position.y, transform.position.z);
                hitRight = false;
        }


        
    }
    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.tag == "Player")
        {
            Invoke("Lose", 1f);
            Destroy(collision.gameObject);
            SceneManager.LoadScene(0);

        }
    }

}
